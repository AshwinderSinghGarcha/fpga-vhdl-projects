--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   08:55:15 12/16/2017
-- Design Name:   
-- Module Name:   E:/ato1mux/ato1muxtestbench.vhd
-- Project Name:  ato1mux
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ato1mux
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY ato1muxtestbench IS
END ato1muxtestbench;
 
ARCHITECTURE behavior OF ato1muxtestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ato1mux
    PORT(
         in1 : IN  std_logic_vector(7 downto 0);
         in2 : IN  std_logic_vector(7 downto 0);
         in3 : IN  std_logic_vector(7 downto 0);
         in4 : IN  std_logic_vector(7 downto 0);
         in5 : IN  std_logic_vector(7 downto 0);
         in6 : IN  std_logic_vector(7 downto 0);
         in7 : IN  std_logic_vector(7 downto 0);
         in8 : IN  std_logic_vector(7 downto 0);
         en : IN  std_logic ;
         sel : IN  std_logic_vector(2 downto 0);
         y : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal in1 : std_logic_vector(7 downto 0) := (others => '0');
   signal in2 : std_logic_vector(7 downto 0) := (others => '0');
   signal in3 : std_logic_vector(7 downto 0) := (others => '0');
   signal in4 : std_logic_vector(7 downto 0) := (others => '0');
   signal in5 : std_logic_vector(7 downto 0) := (others => '0');
   signal in6 : std_logic_vector(7 downto 0) := (others => '0');
   signal in7 : std_logic_vector(7 downto 0) := (others => '0');
   signal in8 : std_logic_vector(7 downto 0) := (others => '0');
   signal en : std_logic:= '0';
   signal sel : std_logic_vector(2 downto 0) := (others => '0');

 	--Outputs
   signal y : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
--   constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ato1mux PORT MAP (
          in1 => in1,
          in2 => in2,
          in3 => in3,
          in4 => in4,
          in5 => in5,
          in6 => in6,
          in7 => in7,
          in8 => in8,
          en => en,
          sel => sel,
          y => y
        );

--   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
wait for 10 ns;	
in1 <= "00000001";
in2 <= "00000010";
in3 <= "00000100";
in4 <= "00001000";
in5 <= "00010000";
in6 <= "00100000";
in7 <= "01000000";
in8 <= "10000000";
en <= '0' ; 


wait for 10 ns;	
in1 <= "00000001";
in2 <= "00000010";
in3 <= "00000100";
in4 <= "00001000";
in5 <= "00010000";
in6 <= "00100000";
in7 <= "01000000";
in8 <= "10000000";
en <= '1' ;
wait for 10 ns;
sel <= "000";
wait for 10 ns;
sel <= "001";
wait for 10 ns;
sel <= "010";
wait for 10 ns;
sel <= "011";
wait for 10 ns;
sel <= "100";
wait for 10 ns;
sel <= "101";
wait for 10 ns;
sel <= "110";
wait for 10 ns;
sel <= "111";
      wait;
   end process;

END;
