----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:44:56 12/16/2017 
-- Design Name: 
-- Module Name:    ato1mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ato1mux is
    Port ( in1 : in  STD_LOGIC_VECTOR (7 downto 0);
           in2 : in  STD_LOGIC_VECTOR (7 downto 0);
           in3 : in  STD_LOGIC_VECTOR (7 downto 0);
           in4 : in  STD_LOGIC_VECTOR (7 downto 0);
           in5 : in  STD_LOGIC_VECTOR (7 downto 0);
           in6 : in  STD_LOGIC_VECTOR (7 downto 0);
           in7 : in  STD_LOGIC_VECTOR (7 downto 0);
           in8 : in  STD_LOGIC_VECTOR (7 downto 0);
           en : in  STD_LOGIC;
           sel : in  STD_LOGIC_VECTOR (2 downto 0);
           y : out  STD_LOGIC_VECTOR (7 downto 0));
end ato1mux;

architecture Behavioral of ato1mux is

begin

process (en ,sel)

begin 


     if (en = '1') then 
                    if (sel = "000") then y<= in1 ;
					  elsif (sel = "001") then y<= in2 ;
                 elsif (sel = "010") then y<= in3 ;
                 elsif (sel = "011") then y<= in4 ;
                 elsif (sel = "100") then y<= in5 ;
                 elsif (sel = "101") then y<= in6 ;
                 elsif (sel = "110") then y<= in7 ;
                 elsif (sel = "111") then y<= in8 ;
                 end if ; 
	 else 	
    y<= "ZZZZZZZZ";	 
      end if ;

end process;


end Behavioral;

