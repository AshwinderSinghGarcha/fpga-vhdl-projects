----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:02:21 12/23/2017 
-- Design Name: 
-- Module Name:    egntbarrelshiftvhdl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity egntbarrelshiftvhdl is
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);
           SHIFT : in  STD_LOGIC_VECTOR (2 downto 0);
           B : out  STD_LOGIC_VECTOR (7 downto 0));
end egntbarrelshiftvhdl;

architecture Behavioral of egntbarrelshiftvhdl is

begin


with SHIFT select 
B<= A(0)         & A(7 DOWNTO 1) WHEN "001",
    A(1 DOWNTO 0)& A(7 DOWNTO 2) WHEN "010",
	 A(2 DOWNTO 0)& A(7 DOWNTO 3) WHEN "011",
	 A(3 DOWNTO 0)& A(7 DOWNTO 4) WHEN "100",
	 A(4 DOWNTO 0)& A(7 DOWNTO 5) WHEN "101",
	 A(5 DOWNTO 0)& A(7 DOWNTO 6) WHEN "110",
	 A(6 DOWNTO 0)& A(7)          WHEN "111",
	 A                WHEN OTHERS;
	 
end Behavioral;

