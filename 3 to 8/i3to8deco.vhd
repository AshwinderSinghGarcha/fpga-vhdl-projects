----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:30:28 12/09/2017 
-- Design Name: 
-- Module Name:    i3to8deco - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i3to8deco is
    Port ( a : in  STD_LOGIC_VECTOR (2 downto 0);
           enb : in  STD_LOGIC;
           y : out  STD_LOGIC_VECTOR (7 downto 0));
end i3to8deco;

architecture Behavioral of i3to8deco is

begin

               process ( a ,enb )
                       begin 
							           
							           if (a="000" and enb ='0') then 
								      y<= "00000000";
								     elsif (a="000" and enb ='1') then 
									  y<= "10000000";
									  elsif (a="001" and enb ='1') then 
									  y<= "01000000";
									  elsif (a="010" and enb ='1') then 
									  y<= "00100000";
									  elsif (a="011" and enb ='1') then 
									  y<= "00010000";
									  elsif (a="100" and enb ='1') then 
									  y<= "00001000";
									  elsif (a="101" and enb ='1') then 
									  y<= "00000100";
									  elsif (a="110" and enb ='1') then 
									  y<= "00000010";
									  elsif (a="111" and enb ='1') then 
									  y<= "00000001";
									  end if ;
                     
              end process;

end Behavioral;

