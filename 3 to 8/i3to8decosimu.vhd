--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:36:24 12/09/2017
-- Design Name:   
-- Module Name:   E:/i3to8deco/i3to8decosimu.vhd
-- Project Name:  i3to8deco
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: i3to8deco
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY i3to8decosimu IS
END i3to8decosimu;
 
ARCHITECTURE behavior OF i3to8decosimu IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT i3to8deco
    PORT(
         a : IN  std_logic_vector(2 downto 0);
         enb : IN  std_logic;
         y : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(2 downto 0) := (others => '0');
   signal enb : std_logic := '0';

 	--Outputs
   signal y : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
-- 
--   constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: i3to8deco PORT MAP (
          a => a,
          enb => enb,
          y => y
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
                          
		                        a<="000"; 
										enb<='1';
	                           wait for 10 ns ;	
								      a<="001";
										enb <='1';
	                           wait for 10 ns ;								   
										a<="010";
										enb <='1'; 
	                           wait for 10 ns ;							  
									   a<="011";
									   enb <='1';
	                           wait for 10 ns ;						  
									   a<="100";
									   enb <='1';
										wait for 10 ns;	
		                        a<="101"; 
										enb<='1';
	                           wait for 10 ns ;	
								      a<="110";
										enb <='1';
	                           wait for 10 ns ;								   
										a<="111";
										enb <='1'; 
	                           
										wait for 10 ns;
										a<="000"; 
										enb<='0';
	                           wait for 10 ns ;	
								      a<="001";
										enb <='0';
	                           wait for 10 ns ;								   
										a<="010";
										enb <='0'; 
	                           wait for 10 ns ;							  
									   a<="011";
									   enb <='0';
	                           wait for 10 ns ;						  
									   a<="100";
									   enb <='0';
										wait for 10 ns;	
		                        a<="101"; 
										enb<='0';
	                           wait for 10 ns ;	
								      a<="110";
										enb <='0';
	                           wait for 10 ns ;								   
										a<="111";
										enb <='0'; 
	                           
										wait for 10 ns ;
                              a<="000"; 
										enb<='1';
	                           wait for 10 ns ;	
								      a<="001";
										enb <='0';
	                           wait for 10 ns ;								   
										a<="010";
										enb <='1'; 
	                           wait for 10 ns ;							  
									   a<="011";
									   enb <='0';
	                           wait for 10 ns ;						  
									   a<="100";
									   enb <='1';
										wait for 10 ns;	
		                        a<="101"; 
										enb<='0';
	                           wait for 10 ns ;	
								      a<="110";
										enb <='1';
	                           wait for 10 ns ;								   
										a<="111";
										enb <='0'; 
      

      -- insert stimulus here 

      wait;
   end process;

END;
