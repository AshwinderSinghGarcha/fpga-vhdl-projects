--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:47:32 12/15/2017
-- Design Name:   
-- Module Name:   E:/forrto1mux/forrto1muxsimu.vhd
-- Project Name:  forrto1mux
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: forrto1mux
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY forrto1muxsimu IS
END forrto1muxsimu;
 
ARCHITECTURE behavior OF forrto1muxsimu IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT forrto1mux
    PORT(
         in1 : IN  std_logic_vector(3 downto 0);
         in2 : IN  std_logic_vector(3 downto 0);
         in3 : IN  std_logic_vector(3 downto 0);
         in4 : IN  std_logic_vector(3 downto 0);
         sel : IN  std_logic_vector(1 downto 0);
         y : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal in1 : std_logic_vector(3 downto 0) := (others => '0');
   signal in2 : std_logic_vector(3 downto 0) := (others => '0');
   signal in3 : std_logic_vector(3 downto 0) := (others => '0');
   signal in4 : std_logic_vector(3 downto 0) := (others => '0');
   signal sel : std_logic_vector(1 downto 0) := (others => '0');

 	--Outputs
   signal y : std_logic_vector(3 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
--   constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: forrto1mux PORT MAP (
          in1 => in1,
          in2 => in2,
          in3 => in3,
          in4 => in4,
          sel => sel,
          y => y
        );

--   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
 



wait for 10 ns ;
in1<="1000";
in2<="1100";
in3<="1110";
in4<="1111";
sel<="00";


wait for 10 ns ;
in1<="1000";
in2<="1100";
in3<="1110";
in4<="1111";
sel<="01";


wait for 10 ns ;
in1<="1000";
in2<="1100";
in3<="1110";
in4<="1111";
sel<="10";

wait for 10 ns;
in1<="1000";
in2<="1100";
in3<="1110";
in4<="1111"; 
sel<="11";


      
      wait;
   end process;

END;
