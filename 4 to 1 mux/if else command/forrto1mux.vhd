----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:46:00 12/15/2017 
-- Design Name: 
-- Module Name:    forrto1mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity forrto1mux is
    Port ( in1 : in  STD_LOGIC_VECTOR (3 downto 0);
           in2 : in  STD_LOGIC_VECTOR (3 downto 0);
           in3 : in  STD_LOGIC_VECTOR (3 downto 0);
           in4 : in  STD_LOGIC_VECTOR (3 downto 0);
           sel : in  STD_LOGIC_VECTOR (1 downto 0);
			  enb : in STD_logic ;
           y : out  STD_LOGIC_VECTOR (3 downto 0));
end forrto1mux;

architecture Behavioral of forrto1mux is

begin
process (sel , enb)
begin
if (enb = '1') then
                    if (sel = "00") then y<= in1 ;
                 elsif (sel = "01") then y<= in2 ;
                 elsif (sel = "10") then y<= in3 ;
					  elsif (sel = "11") then y<= in4 ;
                 end if ;                 

end if;




end process ;			  



end Behavioral;

