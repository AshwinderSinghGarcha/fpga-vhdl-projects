--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:42:05 12/25/2017
-- Design Name:   
-- Module Name:   E:/addsub4bt/addsub4bttestbench.vhd
-- Project Name:  addsub4bt
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: addsub4bt
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY addsub4bttestbench IS
END addsub4bttestbench;
 
ARCHITECTURE behavior OF addsub4bttestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT addsub4bt
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         z : OUT  std_logic_vector(3 downto 0);
         addsub : IN  std_logic;
         overflow : OUT  std_logic;
         cout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');
   signal addsub : std_logic := '0';

 	--Outputs
   signal z : std_logic_vector(3 downto 0);
   signal overflow : std_logic;
   signal cout : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: addsub4bt PORT MAP (
          a => a,
          b => b,
          z => z,
          addsub => addsub,
          overflow => overflow,
          cout => cout
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      	
      wait for 10 ns;	
a<="1111";
b<="0001";
addsub<='1';     
    wait for 10 ns;	
a<="1111";
b<="0011";
addsub<='1';     

    wait for 10 ns;	
a<="1111";
b<="0111";
addsub<='1';     
    wait for 10 ns;	
a<="1111";
b<="1001";
addsub<='1';     
    wait for 10 ns;	
a<="1111";
b<="1101";
addsub<='1';     

    wait for 10 ns;	
a<="1101";
b<="0001";
addsub<='1';
      
		     wait for 10 ns;	
a<="1111";
b<="0001";
addsub<='0';     
    wait for 10 ns;	
a<="1111";
b<="0011";
addsub<='0';     

    wait for 10 ns;	
a<="1111";
b<="0111";
addsub<='0';     
    wait for 10 ns;	
a<="1111";
b<="1001";
addsub<='0';     
    wait for 10 ns;	
a<="1111";
b<="1101";
addsub<='0';     

    wait for 10 ns;	
a<="1101";
b<="0001";
addsub<='0';
      

      wait;
   end process;

END;
