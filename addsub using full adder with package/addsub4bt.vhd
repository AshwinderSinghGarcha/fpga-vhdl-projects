----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:41:26 12/25/2017 
-- Design Name: 
-- Module Name:    addsub4bt - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.my_packagefull_adder.all;

entity addsub4bt is
    Port ( a : in  STD_LOGIC_VECTOR (3 downto 0);
           b : in  STD_LOGIC_VECTOR (3 downto 0);
           z : out  STD_LOGIC_VECTOR (3 downto 0);
           addsub : in  STD_LOGIC;
           overflow : out  STD_LOGIC;
			  cout:out STD_logic);
end addsub4bt;

architecture Behavioral of addsub4bt is
signal p,q,r,s:std_logic ;
signal l,m,n,o:std_logic ;
begin
p<=addsub xor b(3);q<=addsub xor b(2);r<=addsub xor b(1);s<=addsub xor b(0);
f0 :fulladdervhdl port map (a(0),s    ,addsub ,z(0), l);
f1 :fulladdervhdl port map (a(1),r    , l     ,z(1), m);
f2 :fulladdervhdl port map (a(2),q    , m     ,z(2), n);
f3 :fulladdervhdl port map (a(3),p    , n     ,z(3), o);
overflow <= n xor o;
cout <=o;
end Behavioral;

