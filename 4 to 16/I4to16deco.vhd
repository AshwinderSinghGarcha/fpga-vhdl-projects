----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:19:45 12/09/2017 
-- Design Name: 
-- Module Name:    I4to16deco - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity I4to16deco is
    Port ( a0 : in  STD_LOGIC;
           a1 : in  STD_LOGIC;
           a2 : in  STD_LOGIC;
           a3 : in  STD_LOGIC;
           e : in  STD_LOGIC;
			  
           y0 : out  STD_LOGIC;
           y1 : out  STD_LOGIC;
           y2 : out  STD_LOGIC;
           y3 : out  STD_LOGIC;
           y4 : out  STD_LOGIC;
           y5 : out  STD_LOGIC;
           y6 : out  STD_LOGIC;
           y7 : out  STD_LOGIC;
           y8 : out  STD_LOGIC;
           y9 : out  STD_LOGIC;
           y10 : out  STD_LOGIC;
           y11 : out  STD_LOGIC;
           y12 : out  STD_LOGIC;
           y13 : out  STD_LOGIC;
           y14 : out  STD_LOGIC;
           y15 : out  STD_LOGIC);
end I4to16deco;

architecture Behavioral of I4to16deco is

begin
      
       y0<=  (e) and  (not a0) and (not a1) and (not a2) and (not a3) ;
		 y1<=  (e) and  (not a0) and (not a1) and (not a2) and (a3) ;
       y2<=  (e) and  (not a0) and (not a1) and (a2) and (not a3) ;
       y3<=  (e) and  (not a0) and (not a1) and (a2) and (a3) ;
       y4<=  (e) and  (not a0) and (a1) and (not a2) and (not a3) ;
       y5<=  (e) and  (not a0) and (a1) and (not a2) and (a3) ;
       y6<=  (e) and  (not a0) and (a1) and (a2) and (not a3) ;
       y7<=  (e) and  (not a0) and (a1) and (a2) and (a3) ;
       y8<=  (e) and   (a0) and (not a1) and (not a2) and (not a3) ;
       y9<=  (e) and   (a0) and (not a1) and (not a2) and (a3) ;
       y10<= (e) and  (a0) and (not a1) and (a2) and (not a3) ;
       y11<= (e) and  (a0) and (not a1) and (a2) and (a3) ;
       y12<= (e) and  (a0) and (a1) and (not a2) and (not a3) ;
       y13<= (e) and  (a0) and (a1) and (not a2) and (a3) ;
       y14<= (e) and  (a0) and (a1) and (a2) and (not a3) ;
       y15<= (e) and  (a0) and (a1) and (a2) and (a3) ;



end Behavioral;

