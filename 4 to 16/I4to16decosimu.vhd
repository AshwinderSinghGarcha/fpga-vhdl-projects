--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:36:09 12/09/2017
-- Design Name:   
-- Module Name:   E:/I4to16deco/I4to16decosimu.vhd
-- Project Name:  I4to16deco
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: I4to16deco
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY I4to16decosimu IS
END I4to16decosimu;
 
ARCHITECTURE behavior OF I4to16decosimu IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT I4to16deco
    PORT(
         a0 : IN  std_logic;
         a1 : IN  std_logic;
         a2 : IN  std_logic;
         a3 : IN  std_logic;
         e : IN  std_logic;
        
         y0 : OUT  std_logic;
         y1 : OUT  std_logic;
         y2 : OUT  std_logic;
         y3 : OUT  std_logic;
         y4 : OUT  std_logic;
         y5 : OUT  std_logic;
         y6 : OUT  std_logic;
         y7 : OUT  std_logic;
         y8 : OUT  std_logic;
         y9 : OUT  std_logic;
         y10 : OUT  std_logic;
         y11 : OUT  std_logic;
         y12 : OUT  std_logic;
         y13 : OUT  std_logic;
         y14 : OUT  std_logic;
         y15 : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a0 : std_logic := '0';
   signal a1 : std_logic := '0';
   signal a2 : std_logic := '0';
   signal a3 : std_logic := '0';
   signal e : std_logic := '0';

 	--Outputs
 
   signal y0 : std_logic;
   signal y1 : std_logic;
   signal y2 : std_logic;
   signal y3 : std_logic;
   signal y4 : std_logic;
   signal y5 : std_logic;
   signal y6 : std_logic;
   signal y7 : std_logic;
   signal y8 : std_logic;
   signal y9 : std_logic;
   signal y10 : std_logic;
   signal y11 : std_logic;
   signal y12 : std_logic;
   signal y13 : std_logic;
   signal y14 : std_logic;
   signal y15 : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
-- 
--   constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: I4to16deco PORT MAP (
          a0 => a0,
          a1 => a1,
          a2 => a2,
          a3 => a3,
          e => e,
          
          y0 => y0,
          y1 => y1,
          y2 => y2,
          y3 => y3,
          y4 => y4,
          y5 => y5,
          y6 => y6,
          y7 => y7,
          y8 => y8,
          y9 => y9,
          y10 => y10,
          y11 => y11,
          y12 => y12,
          y13 => y13,
          y14 => y14,
          y15 => y15
        );

--   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
       
		                        
		 
                              
		                        a0<= '0';a1<='0'; a2<='0'; a3<='0'; 
										e<='1';
	                           wait for 10 ns ;	
								      a0<= '0';a1<='0'; a2<='0'; a3<='1'; 
										e<='1';
	                           wait for 10 ns ;								   
										a0<= '0';a1<='0'; a2<='1'; a3<='0'; 
										e<='1';
	                           wait for 10 ns ;							  
									   a0<= '0';a1<='0'; a2<='1'; a3<='1'; 
										e<='1';
	                           wait for 10 ns ;						  
									   a0<= '0';a1<='1'; a2<='0'; a3<='0'; 
										e<='1';
										wait for 10 ns;	
		                        a0<= '0';a1<='1'; a2<='0'; a3<='1'; 
										e<='1';
	                           wait for 10 ns ;	
								      a0<= '0';a1<='1'; a2<='1'; a3<='0'; 
										e<='1';
	                           wait for 10 ns ;								   
										a0<= '0';a1<='1'; a2<='1'; a3<='1'; 
										e<='1'; 
	                           
										wait for 10 ns;
										a0<= '1';a1<='0'; a2<='0'; a3<='0'; 
										e<='1'; 
	                           wait for 10 ns ;	
								      a0<= '1';a1<='0'; a2<='0'; a3<='1'; 
										e<='1'; 
	                           wait for 10 ns ;								   
										a0<= '1';a1<='0'; a2<='1'; a3<='0'; 
										e<='1'; 
	                           wait for 10 ns ;							  
									   a0<= '1';a1<='0'; a2<='1'; a3<='1'; 
										e<='1'; 
	                           wait for 10 ns ;						  
									   a0<= '1';a1<='1'; a2<='0'; a3<='0'; 
										e<='1'; 
										wait for 10 ns;	
		                        a0<= '1';a1<='1'; a2<='0'; a3<='1'; 
										e<='1'; 
	                           wait for 10 ns ;	
								      a0<= '1';a1<='1'; a2<='1'; a3<='0'; 
										e<='1'; 
	                           wait for 10 ns ;								   
										a0<= '1';a1<='1'; a2<='1'; a3<='1'; 
										e<='1'; 
	                           
      

      wait;
   end process;

END;
