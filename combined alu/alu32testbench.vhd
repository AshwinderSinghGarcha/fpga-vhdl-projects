--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:24:13 12/31/2017
-- Design Name:   
-- Module Name:   E:/alu32/alu32testbench.vhd
-- Project Name:  alu32
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: alu32
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY alu32testbench IS
END alu32testbench;
 
ARCHITECTURE behavior OF alu32testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT alu32
    PORT(
         al : IN  std_logic_vector(31 downto 0);
         bl : IN  std_logic_vector(31 downto 0);
         enl : IN  std_logic;
         sell : IN  std_logic_vector(3 downto 0);
         yl : OUT  std_logic_vector(31 downto 0);
         carryinl : IN  std_logic;
         carryoutl : OUT  std_logic;
         borrowinl : IN  std_logic;
         borrowoutl : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal al : std_logic_vector(31 downto 0) := (others => '0');
   signal bl : std_logic_vector(31 downto 0) := (others => '0');
   signal enl : std_logic := '0';
   signal sell : std_logic_vector(3 downto 0) := (others => '0');
   signal carryinl : std_logic := '0';
   signal borrowinl : std_logic := '0';

 	--Outputs
   signal yl : std_logic_vector(31 downto 0);
   signal carryoutl : std_logic;
   signal borrowoutl : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   ---constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: alu32 PORT MAP (
          al => al,
          bl => bl,
          enl => enl,
          sell => sell,
          yl => yl,
          carryinl => carryinl,
          carryoutl => carryoutl,
          borrowinl => borrowinl,
          borrowoutl => borrowoutl
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	

al<="00001111000011110000111100001111"; 
bl<="00001111000011110000111100001111";
carryinl<='0';
borrowinl<='0';

enl <= '0' ;
wait for 10 ns;
sell <= "0000";
wait for 10 ns;
sell <= "0001";
wait for 10 ns;
sell <= "0010";
wait for 10 ns;
sell <= "0011";
wait for 10 ns;
sell <= "0100";
wait for 10 ns;
sell <= "0101";
wait for 10 ns;
sell <= "0110";
wait for 10 ns;
sell <= "0111";
wait for 10 ns;
sell <= "1000";
wait for 10 ns;
sell <= "1001";
wait for 10 ns;
sell <= "1010";
wait for 10 ns;
sell <= "1011";
wait for 10 ns;
sell <= "1100";
wait for 10 ns;
sell <= "1101";
wait for 10 ns;
sell <= "1110";


enl <= '1' ;
wait for 10 ns;
sell <= "0000";
wait for 10 ns;
sell <= "0001";
wait for 10 ns;
sell <= "0010";
wait for 10 ns;
sell <= "0011";
wait for 10 ns;
sell <= "0100";
wait for 10 ns;
sell <= "0101";
wait for 10 ns;
sell <= "0110";
wait for 10 ns;
sell <= "0111";
wait for 10 ns;
sell <= "1000";
wait for 10 ns;
sell <= "1001";
wait for 10 ns;
sell <= "1010";
wait for 10 ns;
sell <= "1011";
wait for 10 ns;
sell <= "1100";
wait for 10 ns;
sell <= "1101";
wait for 10 ns;
sell <= "1110";

carryinl<='1';
borrowinl<='0';

enl <= '1' ;
wait for 10 ns;
sell <= "0000";
wait for 10 ns;
sell <= "0001";
wait for 10 ns;
sell <= "0010";
wait for 10 ns;
sell <= "0011";
wait for 10 ns;
sell <= "0100";
wait for 10 ns;
sell <= "0101";
wait for 10 ns;
sell <= "0110";
wait for 10 ns;
sell <= "0111";
wait for 10 ns;
sell <= "1000";
wait for 10 ns;
sell <= "1001";
wait for 10 ns;
sell <= "1010";
wait for 10 ns;
sell <= "1011";
wait for 10 ns;
sell <= "1100";
wait for 10 ns;
sell <= "1101";
wait for 10 ns;
sell <= "1110";

carryinl<='0';
borrowinl<='1';

enl <= '1' ;
wait for 10 ns;
sell <= "0000";
wait for 10 ns;
sell <= "0001";
wait for 10 ns;
sell <= "0010";
wait for 10 ns;
sell <= "0011";
wait for 10 ns;
sell <= "0100";
wait for 10 ns;
sell <= "0101";
wait for 10 ns;
sell <= "0110";
wait for 10 ns;
sell <= "0111";
wait for 10 ns;
sell <= "1000";
wait for 10 ns;
sell <= "1001";
wait for 10 ns;
sell <= "1010";
wait for 10 ns;
sell <= "1011";
wait for 10 ns;
sell <= "1100";
wait for 10 ns;
sell <= "1101";
wait for 10 ns;
sell <= "1110";

      -- insert stimulus here 

      wait;
   end process;

END;
