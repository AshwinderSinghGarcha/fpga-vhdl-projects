----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:00:42 12/31/2017 
-- Design Name: 
-- Module Name:    alu32 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 





---------------------------------------------------15 to 1 mux -----------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity fifto1m is
Port (      in1 : in  STD_LOGIC_VECTOR (31 downto 0);
            in2 : in  STD_LOGIC_VECTOR (31 downto 0);
            in3 : in  STD_LOGIC_VECTOR (31 downto 0);
            in4 : in  STD_LOGIC_VECTOR (31 downto 0);
            in5 : in  STD_LOGIC_VECTOR (31 downto 0);
            in6 : in  STD_LOGIC_VECTOR (31 downto 0);
            in7 : in  STD_LOGIC_VECTOR (31 downto 0);
            in8 : in  STD_LOGIC_VECTOR (31 downto 0);
            in9 : in  STD_LOGIC_VECTOR (31 downto 0);
           in10 : in  STD_LOGIC_VECTOR (31 downto 0);
           in11 : in  STD_LOGIC_VECTOR (31 downto 0);
           in12 : in  STD_LOGIC_VECTOR (31 downto 0);
           in13 : in  STD_LOGIC_VECTOR (31 downto 0);
           in14 : in  STD_LOGIC_VECTOR (31 downto 0);
           in15 : in  STD_LOGIC_VECTOR (31 downto 0);
			  
           sel : in  STD_LOGIC_VECTOR (3 downto 0);
           en : in  STD_LOGIC;
           y : out  STD_LOGIC_VECTOR (31 downto 0));
end fifto1m;

architecture Behavioral of fifto1m is
begin
process (en ,sel)
begin 


     if (en = '1') then 
                    if (sel = "0000") then y<= in1 ;
					  elsif (sel = "0001") then y<= in2 ;
                 elsif (sel = "0010") then y<= in3 ;
                 elsif (sel = "0011") then y<= in4 ;
                 elsif (sel = "0100") then y<= in5 ;
                 elsif (sel = "0101") then y<= in6 ;
                 elsif (sel = "0110") then y<= in7 ;
                 elsif (sel = "0111") then y<= in8 ;
					  elsif (sel = "1000") then y<= in9 ;
					  elsif (sel = "1001") then y<= in10 ;
                 elsif (sel = "1010") then y<= in11;
                 elsif (sel = "1011") then y<= in12 ;
                 elsif (sel = "1100") then y<= in13 ;
                 elsif (sel = "1101") then y<= in14;
                 elsif (sel = "1110") then y<= in15 ;
       
                 end if ; 
	 else 	
    y<= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";	 
     end if ;
end process ;
end Behavioral;




-------------------------------------------------15 to 1 mux end --------------------------------------------









-------------------------------------------------32 bt adder ---------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
-------------------------------------------------full adder start------------------------------------------------------
entity fulladdervhdl is
    Port ( af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
           cinf : in  STD_LOGIC;
           sumf : out  STD_LOGIC;
           coutf : out  STD_LOGIC);
end fulladdervhdl;

architecture Behavioral of fulladdervhdl is

begin
sumf <= af xor bf xor cinf ;
coutf <= ( af and bf ) or (cinf and (af xor bf)) ;

end Behavioral;
-------------------------------------------------end full adder ----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
entity for32bitadder is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           carryin : in  STD_LOGIC;
           carryout : out  STD_LOGIC;
           sum : out  STD_LOGIC_VECTOR (31 downto 0));
end for32bitadder;

architecture Behavioral of for32bitadder is

component fulladdervhdl is
    Port ( af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
           cinf : in  STD_LOGIC;
           sumf : out  STD_LOGIC;
           coutf : out  STD_LOGIC);
end component ;
signal carry: std_logic_vector (32 downto 0);

begin
carry(0) <=carryin;
adder32:
for i in 0 to  31  generate 
for32bitadder :fulladdervhdl port map (af=>a(i),
                            bf=>b(i),
                            cinf=>carry(i),
                            sumf=>sum(i),
                            coutf=>carry(i+1));
				 end generate adder32;
carryout<=carry(32);
end Behavioral;



--------------------------------------------------------32 bt adder end-------------------------------------------





--------------------------------------------------------32 bt sub -------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fullsubusingkmap is
    Port (
           af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
			  binf : in  STD_LOGIC;
           subf : out  STD_LOGIC;
           botf : out  STD_LOGIC);
end fullsubusingkmap;

architecture Behavioral of fullsubusingkmap is

begin
subf <=binf xor  af xor bf ;
botf <= ((not af) and bf ) or ( binf and (af xnor bf));


end Behavioral;
-------------------------------------------------32 bit substractor----------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity for32sub is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           borrowin : in  STD_LOGIC;
           borrowout : out  STD_LOGIC;
           sub : out  STD_LOGIC_VECTOR (31 downto 0));
end for32sub;

architecture Behavioral of for32sub is
signal borrow: std_logic_vector (32 downto 0);
component fullsubusingkmap is
    Port (
           af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
			  binf : in  STD_LOGIC;
           subf : out  STD_LOGIC;
           botf : out  STD_LOGIC);
end component fullsubusingkmap;

begin
borrow(0)<=borrowin;
substractor32bit:
for i in 0 to 31 generate 
ux:  fullsubusingkmap port map (
                                
										  af=>a(i),
										  bf=>b(i),
										  binf=>borrow(i),
										  subf=>sub(i),
										  botf=>borrow(i+1)
										  );

end generate ;

borrowout<= borrow(32);
end Behavioral;

-----------------------------------------------------32 bt sub end --------------------------------------------






---------------------------------------------------32 incemrent a ----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity increment32 is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           incrementa : out  STD_LOGIC_VECTOR (31 downto 0));
end increment32;

architecture Behavioral of increment32 is
signal usgn : unsigned (31 downto 0);
signal slv   : std_logic_vector (31 downto 0);
begin
usgn <= unsigned (a) +1 ;
 slv <= std_logic_vector (usgn);
 incrementa <= slv;

end Behavioral;

----------------------------------------------------increment a end -------------------------------------------------





----------------------------------------------------increment b ------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity increment32b is
    Port ( b : in  STD_LOGIC_VECTOR (31 downto 0);
           incrementb : out  STD_LOGIC_VECTOR (31 downto 0));
end increment32b;

architecture Behavioral of increment32b is
signal usgn : unsigned (31 downto 0);
signal slv   : std_logic_vector (31 downto 0);
begin
usgn <= unsigned(b) +1 ;
slv <= std_logic_vector(usgn);
incrementb <= slv;


end Behavioral;


----------------------------------------------------increment b end ------------------------


------------------------------------------------decrement a --------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity decrement32a is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           decrementa : out  STD_LOGIC_VECTOR (31 downto 0));
end decrement32a;

architecture Behavioral of decrement32a is
signal sgn : signed(31 downto 0);
signal slv   : std_logic_vector (31 downto 0);
begin
sgn <= signed(a) -1 ;
 slv <= std_logic_vector(sgn);
 decrementa <= slv;
end Behavioral;





------------------------------------------------decrement a end --------------------------------------





------------------------------------------------decrement b --------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity decrement32b is
    Port ( b : in  STD_LOGIC_VECTOR (31 downto 0);
           decrementb : out  STD_LOGIC_VECTOR (31 downto 0));
end decrement32b;

architecture Behavioral of decrement32b is
signal sgn : signed(31 downto 0);
signal slv   : std_logic_vector (31 downto 0);
begin
sgn <= signed(b) -1 ;
 slv <= std_logic_vector(sgn);
 decrementb <= slv;

end Behavioral;

------------------------------------------------decrement b end --------------------------------------




------------------------------------------------sftrgt ---------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity sftrgt32 is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           sftrgt : out  STD_LOGIC_VECTOR (31 downto 0));
end sftrgt32;

architecture Behavioral of sftrgt32 is

begin
sftrgt <= a(0) & a(31 downto 1);

end Behavioral;


------------------------------------------------sftrgt end ---------------------------------------------



------------------------------------------------sftlft ---------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity sftlft32 is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           sftlft : out  STD_LOGIC_VECTOR (31 downto 0));
end sftlft32;

architecture Behavioral of sftlft32 is

begin
sftlft<= a(30 downto 0) & a(31) ;

end Behavioral;


------------------------------------------------sftlft end---------------------------------------------




------------------------------------------------logic---------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity logic32bt is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           nota : out  STD_LOGIC_VECTOR (31 downto 0);
           andl : out  STD_LOGIC_VECTOR (31 downto 0);
           nandl : out  STD_LOGIC_VECTOR (31 downto 0);
           orl : out  STD_LOGIC_VECTOR (31 downto 0);
           norl : out  STD_LOGIC_VECTOR (31 downto 0);
           xorl : out  STD_LOGIC_VECTOR (31 downto 0);
           xnorl : out  STD_LOGIC_VECTOR (31 downto 0));
end logic32bt;

architecture Behavioral of logic32bt is

begin

           nota <= (not a(31 downto 0) );
           andl <=(a(31 downto 0) and   b(31 downto 0));
           nandl<=(a(31 downto 0) nand  b(31 downto 0));
           orl  <=(a(31 downto 0) or    b(31 downto 0));
           norl <=(a(31 downto 0) nor   b(31 downto 0));
           xorl <=(a(31 downto 0) xor   b(31 downto 0));
           xnorl<=(a(31 downto 0) xnor  b(31 downto 0));

end Behavioral;


------------------------------------------------logic end---------------------------------------------







----------------------------------------alu 32 bt------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity alu32 is
    Port ( al : in  STD_LOGIC_VECTOR (31 downto 0);
           bl : in  STD_LOGIC_VECTOR (31 downto 0);
           enl : in  STD_LOGIC;
           sell : in  STD_LOGIC_VECTOR (3 downto 0);
           yl : out  STD_LOGIC_VECTOR (31 downto 0);
			  carryinl : in  STD_LOGIC;
           carryoutl : out  STD_LOGIC;
			  borrowinl : in  STD_LOGIC;
           borrowoutl : out  STD_LOGIC);
end alu32;

architecture Behavioral of alu32 is



component fifto1m is
Port (      in1 : in  STD_LOGIC_VECTOR (31 downto 0);
            in2 : in  STD_LOGIC_VECTOR (31 downto 0);
            in3 : in  STD_LOGIC_VECTOR (31 downto 0);
            in4 : in  STD_LOGIC_VECTOR (31 downto 0);
            in5 : in  STD_LOGIC_VECTOR (31 downto 0);
            in6 : in  STD_LOGIC_VECTOR (31 downto 0);
            in7 : in  STD_LOGIC_VECTOR (31 downto 0);
            in8 : in  STD_LOGIC_VECTOR (31 downto 0);
            in9 : in  STD_LOGIC_VECTOR (31 downto 0);
           in10 : in  STD_LOGIC_VECTOR (31 downto 0);
           in11 : in  STD_LOGIC_VECTOR (31 downto 0);
           in12 : in  STD_LOGIC_VECTOR (31 downto 0);
           in13 : in  STD_LOGIC_VECTOR (31 downto 0);
           in14 : in  STD_LOGIC_VECTOR (31 downto 0);
           in15 : in  STD_LOGIC_VECTOR (31 downto 0);
			  
           sel : in  STD_LOGIC_VECTOR (3 downto 0);
           en : in  STD_LOGIC;
           y : out  STD_LOGIC_VECTOR (31 downto 0));
end component fifto1m;


component for32bitadder is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           carryin : in  STD_LOGIC;
           carryout : out  STD_LOGIC;
           sum : out  STD_LOGIC_VECTOR (31 downto 0));
end component for32bitadder;


component for32sub is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           borrowin : in  STD_LOGIC;
           borrowout : out  STD_LOGIC;
           sub : out  STD_LOGIC_VECTOR (31 downto 0));
end component for32sub;



component increment32 is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           incrementa : out  STD_LOGIC_VECTOR (31 downto 0));
end component increment32;


component increment32b is
    Port ( b : in  STD_LOGIC_VECTOR (31 downto 0);
           incrementb : out  STD_LOGIC_VECTOR (31 downto 0));
end component increment32b;



component decrement32a is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           decrementa : out  STD_LOGIC_VECTOR (31 downto 0));
end component decrement32a;



component decrement32b is
    Port ( b : in  STD_LOGIC_VECTOR (31 downto 0);
           decrementb : out  STD_LOGIC_VECTOR (31 downto 0));
end component decrement32b;



component sftrgt32 is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           sftrgt : out  STD_LOGIC_VECTOR (31 downto 0));
end component sftrgt32;



component sftlft32 is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           sftlft : out  STD_LOGIC_VECTOR (31 downto 0));
end component sftlft32;


component logic32bt is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           nota : out  STD_LOGIC_VECTOR (31 downto 0);
           andl : out  STD_LOGIC_VECTOR (31 downto 0);
           nandl : out  STD_LOGIC_VECTOR (31 downto 0);
           orl : out  STD_LOGIC_VECTOR (31 downto 0);
           norl : out  STD_LOGIC_VECTOR (31 downto 0);
           xorl : out  STD_LOGIC_VECTOR (31 downto 0);
           xnorl : out  STD_LOGIC_VECTOR (31 downto 0));
end component logic32bt;


signal s1 : std_logic_vector (31 downto 0);
signal s2 : std_logic_vector (31 downto 0);
signal s3 : std_logic_vector (31 downto 0);
signal s4 : std_logic_vector (31 downto 0);
signal s5 : std_logic_vector (31 downto 0);
signal s6 : std_logic_vector (31 downto 0);
signal s7 : std_logic_vector (31 downto 0);
signal s8 : std_logic_vector (31 downto 0);
signal s9 : std_logic_vector (31 downto 0);
signal s10 : std_logic_vector (31 downto 0);
signal s11 : std_logic_vector (31 downto 0);
signal s12 : std_logic_vector (31 downto 0);
signal s13 : std_logic_vector (31 downto 0);
signal s14 : std_logic_vector (31 downto 0);
signal s15 : std_logic_vector (31 downto 0);

begin



u1:for32bitadder  port map (a=>al,b=>bl,sum=>s1,carryin=>carryinl,carryout=>carryoutl);
u2:for32sub       port map (a=>al,b=>bl,sub=>s2,borrowin=>borrowinl,borrowout=>borrowoutl);
u3:increment32    port map (a=>al,incrementa=>s3);
u4:increment32b   port map (b=>bl,incrementb=>s4);
u5:decrement32a   port map (a=>al,decrementa=>s5);
u6:decrement32b   port map (b=>bl,decrementb=>s6);
u7:sftlft32       port map (a=>al,sftlft=>s7);
u8:sftrgt32       port map (a=>al,sftrgt=>s8);
u9:logic32bt      port map (a=>al,b=>bl ,nota=>s9,andl=>s10,nandl=>s11,orl=>s12,norl=>s13,xorl=>s14,xnorl=>s15);
u10:fifto1m       port map (in1=>s1,in2=>s2,in3=>s3,in4=>s4,in5=>s5,in6=>s6,in7=>s7
                            ,in8=>s8,in9=>s9,in10=>s10,in11=>s11,in12=>s12,in13=>s13,in14=>s14
									 ,in15=>s15,sel=>sell,en=>enl,y=>yl);
            
			  
          



end Behavioral;

----------------------------------------alu 32 bt end ---------------------------------------