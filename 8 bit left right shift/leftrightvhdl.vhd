----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:57:32 12/23/2017 
-- Design Name: 
-- Module Name:    leftrightvhdl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity leftrightvhdl is
    Port ( A: in  STD_LOGIC_VECTOR (7 downto 0);
           AMT : in  STD_LOGIC_VECTOR (2 downto 0);
           LR : in  STD_LOGIC;
           Y : out  STD_LOGIC_VECTOR (7 downto 0));
end leftrightvhdl;

architecture Behavioral of leftrightvhdl is

begin
process (LR , AMT)
begin 
if LR = '1' then                               -- right shift

 if (AMT ="001") then
       Y<= A(0)& A(7 DOWNTO 1) ;
    elsif (AMT ="010") THEN
	    y<= A(1 DOWNTO 0)& A(7 DOWNTO 2) ;
	 elsif (AMT ="011") THEN
	    y<= A(2 DOWNTO 0)& A(7 DOWNTO 3)  ;
	 elsif (AMT ="100") THEN
	    Y<=A(3 DOWNTO 0)& A(7 DOWNTO 4) ;
	 elsif (AMT ="101") THEN
	    Y<=A(4 DOWNTO 0)& A(7 DOWNTO 5) ;
	 elsif (AMT ="110") THEN
	    y<= A(5 DOWNTO 0)& A(7 DOWNTO 6) ;
    elsif (AMT ="111") THEN
	    y<= A(6 DOWNTO 0)& A(7) ;	
	 else 
       y<= A ;	 
	 end if ;
else                                             -- left shift
if (AMT ="111") then
       Y<= A(0)& A(7 DOWNTO 1) ;
    elsif (AMT ="110") THEN
	    y<= A(1 DOWNTO 0)& A(7 DOWNTO 2) ;
	 elsif (AMT ="101") THEN
	    y<= A(2 DOWNTO 0)& A(7 DOWNTO 3)  ;
	 elsif (AMT ="100") THEN
	    Y<=A(3 DOWNTO 0)& A(7 DOWNTO 4) ;
	 elsif (AMT ="011") THEN
	    Y<=A(4 DOWNTO 0)& A(7 DOWNTO 5) ;
	 elsif (AMT ="010") THEN
	    y<= A(5 DOWNTO 0)& A(7 DOWNTO 6) ;
    elsif (AMT ="001") THEN
	    y<= A(6 DOWNTO 0)& A(7) ;	
	 else 
       y<= A ;	
    end if ;
   
end if ;
end process ;	 



end Behavioral;

