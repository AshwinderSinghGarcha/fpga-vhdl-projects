--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:47:06 12/23/2017
-- Design Name:   
-- Module Name:   E:/leftright/leftrighttestbench.vhd
-- Project Name:  leftright
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: leftrightvhdl
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY leftrighttestbench IS
END leftrighttestbench;
 
ARCHITECTURE behavior OF leftrighttestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT leftrightvhdl
    PORT(
         A : IN  std_logic_vector(7 downto 0);
         AMT : IN  std_logic_vector(2 downto 0);
         LR : IN  std_logic;
         Y : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(7 downto 0) := (others => '0');
   signal AMT : std_logic_vector(2 downto 0) := (others => '0');
   signal LR : std_logic := '0';

 	--Outputs
   signal Y : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: leftrightvhdl PORT MAP (
          A => A,
          AMT => AMT,
          LR => LR,
          Y => Y
        );

--   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;
A<="10000000"; 	
LR<='1';
wait for 10 ns;
AMT<="000";
wait for 10 ns;
AMT<="001";
wait for 10 ns;
AMT<="010";
wait for 10 ns;     
AMT<="011"; 
wait for 10 ns;
AMT<="100";
wait for 10 ns;
AMT<="101";
wait for 10 ns;
AMT<="110";
wait for 10 ns;
AMT<="111";
wait for 10 ns;

A<="10000000";
LR<='0';
wait for 10 ns;
AMT<="000";
wait for 10 ns;
AMT<="001";
wait for 10 ns;
AMT<="010";
wait for 10 ns;     
AMT<="011"; 
wait for 10 ns;
AMT<="100";
wait for 10 ns;
AMT<="101";
wait for 10 ns;
AMT<="110";
wait for 10 ns;
AMT<="111";
wait for 10 ns;



     

      wait;
   end process;

END;
