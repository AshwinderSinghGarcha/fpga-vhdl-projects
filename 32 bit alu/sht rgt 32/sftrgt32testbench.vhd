--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:19:12 12/30/2017
-- Design Name:   
-- Module Name:   E:/sftrgt32/sftrgt32testbench.vhd
-- Project Name:  sftrgt32
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: sftrgt32
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY sftrgt32testbench IS
END sftrgt32testbench;
 
ARCHITECTURE behavior OF sftrgt32testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sftrgt32
    PORT(
         a : IN  std_logic_vector(31 downto 0);
         sftrgt : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(31 downto 0) := (others => '0');

 	--Outputs
   signal sftrgt : std_logic_vector(31 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sftrgt32 PORT MAP (
          a => a,
          sftrgt => sftrgt
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
		
		a<="11111111111111111111111111111100";
wait for 10 ns;	
		
		a<="00111111111111111111111111111111";
wait for 10 ns;	
		
		a<="11011111111111111111111111111100";

      

      -- insert stimulus here 

      wait;
   end process;

END;
