----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:17:29 12/30/2017 
-- Design Name: 
-- Module Name:    sftrgt32 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sftrgt32 is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           sftrgt : out  STD_LOGIC_VECTOR (31 downto 0));
end sftrgt32;

architecture Behavioral of sftrgt32 is

begin
sftrgt <= a(0) & a(31 downto 1);

end Behavioral;

