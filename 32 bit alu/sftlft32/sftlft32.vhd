----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:31:29 12/30/2017 
-- Design Name: 
-- Module Name:    sftlft32 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sftlft32 is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           sftlft : out  STD_LOGIC_VECTOR (31 downto 0));
end sftlft32;

architecture Behavioral of sftlft32 is

begin
sftlft<= a(30 downto 0) & a(31) ;

end Behavioral;

