--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:15:35 12/31/2017
-- Design Name:   
-- Module Name:   E:/increment32b/increment32btestbench.vhd
-- Project Name:  increment32b
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: increment32b
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY increment32btestbench IS
END increment32btestbench;
 
ARCHITECTURE behavior OF increment32btestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT increment32b
    PORT(
         b : IN  std_logic_vector(31 downto 0);
         incrementb : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal b : std_logic_vector(31 downto 0) := (others => '0');

 	--Outputs
   signal incrementb: std_logic_vector(31 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   ---constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: increment32b PORT MAP (
          b => b,
          incrementb => incrementb
        );

   -- Clock process definitions
   --<clock>_process :process
   --begin
	--	<clock> <= '0';
		--wait for <clock>_period/2;
		--<clock> <= '1';
		--wait for <clock>_period/2;
   --end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	

    b<="00001111000011110000111100001111";

      -- insert stimulus here 

      wait;
   end process;

END;
