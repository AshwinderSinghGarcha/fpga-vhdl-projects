----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:10:29 12/31/2017 
-- Design Name: 
-- Module Name:    increment32b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity increment32b is
    Port ( b : in  STD_LOGIC_VECTOR (31 downto 0);
           incrementb : out  STD_LOGIC_VECTOR (31 downto 0));
end increment32b;

architecture Behavioral of increment32b is
signal usgn : unsigned (31 downto 0);
signal slv   : std_logic_vector (31 downto 0);
begin
usgn <= unsigned(b) +1 ;
slv <= std_logic_vector(usgn);
incrementb <= slv;


end Behavioral;

