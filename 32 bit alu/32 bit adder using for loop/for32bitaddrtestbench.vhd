--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:45:56 12/30/2017
-- Design Name:   
-- Module Name:   E:/for32bitaddr/for32bitaddrtestbench.vhd
-- Project Name:  for32bitaddr
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: for32bitadder
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY for32bitaddrtestbench IS
END for32bitaddrtestbench;
 
ARCHITECTURE behavior OF for32bitaddrtestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT for32bitadder
    PORT(
         a : IN  std_logic_vector(31 downto 0);
         b : IN  std_logic_vector(31 downto 0);
         carryin : IN  std_logic;
         carryout : OUT  std_logic;
         sum : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(31 downto 0) := (others => '0');
   signal b : std_logic_vector(31 downto 0) := (others => '0');
   signal carryin : std_logic := '0';

 	--Outputs
   signal carryout : std_logic;
   signal sum : std_logic_vector(31 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: for32bitadder PORT MAP (
          a => a,
          b => b,
          carryin => carryin,
          carryout => carryout,
          sum => sum
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	

      a<="00000000111111110000000011111111";
		b<="00000000111111110000000011111111";
		carryin<='1';
		

      -- insert stimulus here 

      wait;
   end process;

END;
