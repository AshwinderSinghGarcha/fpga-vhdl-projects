----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:16:10 12/30/2017 
-- Design Name: 
-- Module Name:    for32bitadder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;


-------------------------------------------------full adder start------------------------------------------------------
entity fulladdervhdl is
    Port ( af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
           cinf : in  STD_LOGIC;
           sumf : out  STD_LOGIC;
           coutf : out  STD_LOGIC);
end fulladdervhdl;

architecture Behavioral of fulladdervhdl is

begin
sumf <= af xor bf xor cinf ;
coutf <= ( af and bf ) or (cinf and (af xor bf)) ;

end Behavioral;
-------------------------------------------------end full adder ----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
entity for32bitadder is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           carryin : in  STD_LOGIC;
           carryout : out  STD_LOGIC;
           sum : out  STD_LOGIC_VECTOR (31 downto 0));
end for32bitadder;

architecture Behavioral of for32bitadder is

component fulladdervhdl is
    Port ( af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
           cinf : in  STD_LOGIC;
           sumf : out  STD_LOGIC;
           coutf : out  STD_LOGIC);
end component ;
signal carry: std_logic_vector (32 downto 0);

begin
carry(0) <=carryin;
adder32:
for i in 0 to  31  generate 
for32bitadder :fulladdervhdl port map (af=>a(i),
                            bf=>b(i),
                            cinf=>carry(i),
                            sumf=>sum(i),
                            coutf=>carry(i+1));
				 end generate adder32;
carryout<=carry(32);
end Behavioral;

