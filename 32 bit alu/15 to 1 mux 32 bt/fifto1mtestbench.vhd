--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:34:21 12/31/2017
-- Design Name:   
-- Module Name:   E:/fifto1m/fifto1mtestbench.vhd
-- Project Name:  fifto1m
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: fifto1m
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY fifto1mtestbench IS
END fifto1mtestbench;
 
ARCHITECTURE behavior OF fifto1mtestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT fifto1m
    PORT(
         in1 : IN  std_logic_vector(31 downto 0);
         in2 : IN  std_logic_vector(31 downto 0);
         in3 : IN  std_logic_vector(31 downto 0);
         in4 : IN  std_logic_vector(31 downto 0);
         in5 : IN  std_logic_vector(31 downto 0);
         in6 : IN  std_logic_vector(31 downto 0);
         in7 : IN  std_logic_vector(31 downto 0);
         in8 : IN  std_logic_vector(31 downto 0);
         in9 : IN  std_logic_vector(31 downto 0);
         in10 : IN  std_logic_vector(31 downto 0);
         in11 : IN  std_logic_vector(31 downto 0);
         in12 : IN  std_logic_vector(31 downto 0);
         in13 : IN  std_logic_vector(31 downto 0);
         in14 : IN  std_logic_vector(31 downto 0);
         in15 : IN  std_logic_vector(31 downto 0);
         sel : IN  std_logic_vector(3 downto 0);
         en : IN  std_logic;
         y : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal in1 : std_logic_vector(31 downto 0) := (others => '0');
   signal in2 : std_logic_vector(31 downto 0) := (others => '0');
   signal in3 : std_logic_vector(31 downto 0) := (others => '0');
   signal in4 : std_logic_vector(31 downto 0) := (others => '0');
   signal in5 : std_logic_vector(31 downto 0) := (others => '0');
   signal in6 : std_logic_vector(31 downto 0) := (others => '0');
   signal in7 : std_logic_vector(31 downto 0) := (others => '0');
   signal in8 : std_logic_vector(31 downto 0) := (others => '0');
   signal in9 : std_logic_vector(31 downto 0) := (others => '0');
   signal in10 : std_logic_vector(31 downto 0) := (others => '0');
   signal in11 : std_logic_vector(31 downto 0) := (others => '0');
   signal in12 : std_logic_vector(31 downto 0) := (others => '0');
   signal in13 : std_logic_vector(31 downto 0) := (others => '0');
   signal in14 : std_logic_vector(31 downto 0) := (others => '0');
   signal in15 : std_logic_vector(31 downto 0) := (others => '0');
   signal sel : std_logic_vector(3 downto 0) := (others => '0');
   signal en : std_logic := '0';

 	--Outputs
   signal y : std_logic_vector(31 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: fifto1m PORT MAP (
          in1 => in1,
          in2 => in2,
          in3 => in3,
          in4 => in4,
          in5 => in5,
          in6 => in6,
          in7 => in7,
          in8 => in8,
          in9 => in9,
          in10 => in10,
          in11 => in11,
          in12 => in12,
          in13 => in13,
          in14 => in14,
          in15 => in15,
          sel => sel,
          en => en,
          y => y
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	

    in1 <= "00000000000000010000000000000001";
in2 <= "00000000000000100000000000000001";
in3 <= "00000000000001000000000000000001";
in4 <= "00000000000010000000000000000001";
in5 <= "00000000000100000000000000000001";
in6 <= "00000000001000000000000000000001";
in7 <= "00000000010000000000000000000001";
in8 <= "00000000100000000000000000000001";
in9 <= "00000001000000000000000000000001";
in10 <= "00000010000000000000000000000001";
in11<= "00000100000000000000000000000001";
in12 <= "00001000000000000000000000000001";
in13<= "00010000000000000000000000000001";
in14<= "00100000000000000000000000000001";
in15 <= "01000000000000000000000000000001";

en <= '0' ; 


wait for 10 ns;	
in1 <= "00000000000000010000000000000001";
in2 <= "00000000000000100000000000000001";
in3 <= "00000000000001000000000000000001";
in4 <= "00000000000010000000000000000001";
in5 <= "00000000000100000000000000000001";
in6 <= "00000000001000000000000000000001";
in7 <= "00000000010000000000000000000001";
in8 <= "00000000100000000000000000000001";
in9 <= "00000001000000000000000000000001";
in10 <= "00000010000000000000000000000001";
in11<= "00000100000000000000000000000001";
in12 <= "00001000000000000000000000000001";
in13<= "00010000000000000000000000000001";
in14<= "00100000000000000000000000000001";
in15 <= "01000000000000000000000000000001";

en <= '1' ;
wait for 10 ns;
sel <= "0000";
wait for 10 ns;
sel <= "0001";
wait for 10 ns;
sel <= "0010";
wait for 10 ns;
sel <= "0011";
wait for 10 ns;
sel <= "0100";
wait for 10 ns;
sel <= "0101";
wait for 10 ns;
sel <= "0110";
wait for 10 ns;
sel <= "0111";
wait for 10 ns;
sel <= "1000";
wait for 10 ns;
sel <= "1001";
wait for 10 ns;
sel <= "1010";
wait for 10 ns;
sel <= "1011";
wait for 10 ns;
sel <= "1100";
wait for 10 ns;
sel <= "1101";
wait for 10 ns;
sel <= "1110";

      

      -- insert stimulus here 

      wait;
   end process;

END;
