----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:48:10 12/30/2017 
-- Design Name: 
-- Module Name:    logic32bt - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity logic32bt is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           nota : out  STD_LOGIC_VECTOR (31 downto 0);
           andl : out  STD_LOGIC_VECTOR (31 downto 0);
           nandl : out  STD_LOGIC_VECTOR (31 downto 0);
           orl : out  STD_LOGIC_VECTOR (31 downto 0);
           norl : out  STD_LOGIC_VECTOR (31 downto 0);
           xorl : out  STD_LOGIC_VECTOR (31 downto 0);
           xnorl : out  STD_LOGIC_VECTOR (31 downto 0));
end logic32bt;

architecture Behavioral of logic32bt is

begin

           nota <= (not a(31 downto 0) );
           andl <=(a(31 downto 0) and   b(31 downto 0));
           nandl<=(a(31 downto 0) nand  b(31 downto 0));
           orl  <=(a(31 downto 0) or    b(31 downto 0));
           norl <=(a(31 downto 0) nor   b(31 downto 0));
           xorl <=(a(31 downto 0) xor   b(31 downto 0));
           xnorl<=(a(31 downto 0) xnor  b(31 downto 0));

end Behavioral;

