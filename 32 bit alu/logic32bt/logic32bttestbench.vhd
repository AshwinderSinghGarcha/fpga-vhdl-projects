--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:53:18 12/30/2017
-- Design Name:   
-- Module Name:   E:/logic32bt/logic32bttestbench.vhd
-- Project Name:  logic32bt
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: logic32bt
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY logic32bttestbench IS
END logic32bttestbench;
 
ARCHITECTURE behavior OF logic32bttestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT logic32bt
    PORT(
         a : IN  std_logic_vector(31 downto 0);
         b : IN  std_logic_vector(31 downto 0);
         nota : OUT  std_logic_vector(31 downto 0);
         andl : OUT  std_logic_vector(31 downto 0);
         nandl : OUT  std_logic_vector(31 downto 0);
         orl : OUT  std_logic_vector(31 downto 0);
         norl : OUT  std_logic_vector(31 downto 0);
         xorl : OUT  std_logic_vector(31 downto 0);
         xnorl : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(31 downto 0) := (others => '0');
   signal b : std_logic_vector(31 downto 0) := (others => '0');

 	--Outputs
   signal nota : std_logic_vector(31 downto 0);
   signal andl : std_logic_vector(31 downto 0);
   signal nandl : std_logic_vector(31 downto 0);
   signal orl : std_logic_vector(31 downto 0);
   signal norl : std_logic_vector(31 downto 0);
   signal xorl : std_logic_vector(31 downto 0);
   signal xnorl : std_logic_vector(31 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: logic32bt PORT MAP (
          a => a,
          b => b,
          nota => nota,
          andl => andl,
          nandl => nandl,
          orl => orl,
          norl => norl,
          xorl => xorl,
          xnorl => xnorl
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	

a<="00111111111111111111111111111111";   
b<="11011111111111111111111111111100";
      wait;
   end process;

END;
