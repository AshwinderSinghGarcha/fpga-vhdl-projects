----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:32:15 12/31/2017 
-- Design Name: 
-- Module Name:    increment32 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.std_logic_arith.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity increment32 is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           incrementa : out  STD_LOGIC_VECTOR (31 downto 0));
end increment32;

architecture Behavioral of increment32 is
signal usgn : unsigned (31 downto 0);
signal slv   : std_logic_vector (31 downto 0);
begin
usgn <= unsigned (a) +1 ;
 slv <= std_logic_vector (usgn);
 incrementa <= slv;

end Behavioral;

