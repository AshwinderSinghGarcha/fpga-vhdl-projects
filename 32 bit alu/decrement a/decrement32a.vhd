----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:43:18 12/31/2017 
-- Design Name: 
-- Module Name:    decrement32a - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decrement32a is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           decrementa : out  STD_LOGIC_VECTOR (31 downto 0));
end decrement32a;

architecture Behavioral of decrement32a is
signal sgn : signed(31 downto 0);
signal slv   : std_logic_vector (31 downto 0);
begin
sgn <= signed(a) -1 ;
 slv <= std_logic_vector(sgn);
 decrementa <= slv;
end Behavioral;

