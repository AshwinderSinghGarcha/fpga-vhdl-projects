----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:01:42 12/31/2017 
-- Design Name: 
-- Module Name:    decrement32b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decrement32b is
    Port ( b : in  STD_LOGIC_VECTOR (31 downto 0);
           decrementb : out  STD_LOGIC_VECTOR (31 downto 0));
end decrement32b;

architecture Behavioral of decrement32b is
signal sgn : signed(31 downto 0);
signal slv   : std_logic_vector (31 downto 0);
begin
sgn <= signed(b) -1 ;
 slv <= std_logic_vector(sgn);
 decrementb <= slv;

end Behavioral;

