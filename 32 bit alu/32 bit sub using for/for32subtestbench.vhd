--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:14:04 12/30/2017
-- Design Name:   
-- Module Name:   E:/for32sub/for32subtestbench.vhd
-- Project Name:  for32sub
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: for32sub
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY for32subtestbench IS
END for32subtestbench;
 
ARCHITECTURE behavior OF for32subtestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT for32sub
    PORT(
         a : IN  std_logic_vector(31 downto 0);
         b : IN  std_logic_vector(31 downto 0);
         borrowin : IN  std_logic;
         borrowout : OUT  std_logic;
         sub : OUT  std_logic_vector(31 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(31 downto 0) := (others => '0');
   signal b : std_logic_vector(31 downto 0) := (others => '0');
   signal borrowin : std_logic := '0';

 	--Outputs
   signal borrowout : std_logic;
   signal sub : std_logic_vector(31 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: for32sub PORT MAP (
          a => a,
          b => b,
          borrowin => borrowin,
          borrowout => borrowout,
          sub => sub
        );

   -- Clock process definitions
   --<clock>_process :process
   ---begin
		--<clock> <= '0';
		--wait for <clock>_period/2;
		--<clock> <= '1';
	--	w-ait for <clock>_period/2;
   --end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	

      a<="00000000111111110000000011111111";
		b<="00000000111111110000000011111111";
		borrowin<='1';
		
		
    
		
      wait;
   end process;

END;
