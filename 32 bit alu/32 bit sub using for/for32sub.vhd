----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:58:26 12/30/2017 
-- Design Name: 
-- Module Name:    for32sub - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fullsubusingkmap is
    Port (
           af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
			  binf : in  STD_LOGIC;
           subf : out  STD_LOGIC;
           botf : out  STD_LOGIC);
end fullsubusingkmap;

architecture Behavioral of fullsubusingkmap is

begin
subf <=binf xor  af xor bf ;
botf <= ((not af) and bf ) or ( binf and (af xnor bf));


end Behavioral;



-------------------------------------------------32 bit substractor----------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity for32sub is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           borrowin : in  STD_LOGIC;
           borrowout : out  STD_LOGIC;
           sub : out  STD_LOGIC_VECTOR (31 downto 0));
end for32sub;

architecture Behavioral of for32sub is
signal borrow: std_logic_vector (32 downto 0);
component fullsubusingkmap is
    Port (
           af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
			  binf : in  STD_LOGIC;
           subf : out  STD_LOGIC;
           botf : out  STD_LOGIC);
end component fullsubusingkmap;

begin
borrow(0)<=borrowin;
substractor32bit:
for i in 0 to 31 generate 
ux:  fullsubusingkmap port map (
                                
										  af=>a(i),
										  bf=>b(i),
										  binf=>borrow(i),
										  subf=>sub(i),
										  botf=>borrow(i+1)
										  );

end generate ;

borrowout<= borrow(32);
end Behavioral;

