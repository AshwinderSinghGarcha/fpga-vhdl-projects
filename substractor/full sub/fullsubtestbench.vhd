--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:36:43 12/24/2017
-- Design Name:   
-- Module Name:   E:/fullsub/fullsubtestbench.vhd
-- Project Name:  fullsub
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: fullsub
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY fullsubtestbench IS
END fullsubtestbench;
 
ARCHITECTURE behavior OF fullsubtestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT fullsub
    PORT(
         af : IN  std_logic;
         bf : IN  std_logic;
         fsub : OUT  std_logic;
         fborrowin : IN  std_logic;
         fborrowout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal af : std_logic := '0';
   signal bf : std_logic := '0';
   signal fborrowin : std_logic := '0';

 	--Outputs
   signal fsub : std_logic;
   signal fborrowout : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: fullsub PORT MAP (
          af => af,
          bf => bf,
          fsub => fsub,
          fborrowin => fborrowin,
          fborrowout => fborrowout
        );
--
--   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
af       <='0';
bf       <='0';
fborrowin<='0'; 
wait for 10 ns;	
af       <='1';
bf       <='0';
fborrowin<='0'; 
      
		wait for 10 ns;	
af       <='0';
bf       <='1';
fborrowin<='0'; 
      
		wait for 10 ns;	
af       <='1';
bf       <='1';
fborrowin<='0'; 
      wait for 10 ns;	
af       <='0';
bf       <='0';
fborrowin<='1'; 
      wait for 10 ns;	
af       <='1';
bf       <='0';
fborrowin<='1'; 
      wait for 10 ns;	
af       <='0';
bf       <='1';
fborrowin<='1'; 
      wait for 10 ns;	
af       <='1';
bf       <='1';
fborrowin<='1'; 
      
      

      -- insert stimulus here 

      wait;
   end process;

END;
