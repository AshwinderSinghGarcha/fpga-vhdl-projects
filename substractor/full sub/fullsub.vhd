----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:35:10 12/24/2017 
-- Design Name: 
-- Module Name:    fullsub - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity halfsub is
    Port ( ah : in  STD_LOGIC;
           bh : in  STD_LOGIC;
           hsub : out  STD_LOGIC;
           hborrow : out  STD_LOGIC);
end halfsub;

architecture Behavioral of halfsub is

begin
hsub <= ah xor bh ;
hborrow <= (not ah) and bh ; 

end Behavioral;

---------------------------------------------end half sub----------------------------------------------------


---------------------------------------------fullsub---------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity fullsub is
    Port ( af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
           fsub : out  STD_LOGIC;
      fborrowin : in std_logic ;
           fborrowout: out  STD_LOGIC);
end fullsub;

architecture Behavioral of fullsub is

signal x,y,z:std_logic ;
component halfsub is 
port (     ah : in  STD_LOGIC;
           bh : in  STD_LOGIC;
           hsub : out  STD_LOGIC;
           hborrow : out  STD_LOGIC);
end component halfsub;

begin
ho : halfsub port map (ah=>af, bh=>bf, hsub=>x, hborrow=>y);
h1 : halfsub port map (ah=>x, bh=>fborrowin, hsub=>fsub, hborrow=>z);
fborrowout <= z or y ;

end Behavioral;