----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:09:27 12/24/2017 
-- Design Name: 
-- Module Name:    halfsub - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-------------------------------------------------halfsub----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity halfsub is
    Port ( ah : in  STD_LOGIC;
           bh : in  STD_LOGIC;
           hsub : out  STD_LOGIC;
           hborrow : out  STD_LOGIC);
end halfsub;

architecture Behavioral of halfsub is

begin
hsub <= ah xor bh ;
hborrow <= (not ah) and bh ; 

end Behavioral;

---------------------------------------------end half sub----------------------------------------------------


---------------------------------------------fullsub---------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity fullsub is
    Port ( af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
           fsub : out  STD_LOGIC;
			  fborrowin : in std_logic ;
           fborrowout: out  STD_LOGIC);
end fullsub;

architecture Behavioral of fullsub is

signal x,y,z:std_logic ;
component halfsub is 
port (     ah : in  STD_LOGIC;
           bh : in  STD_LOGIC;
           hsub : out  STD_LOGIC;
           hborrow : out  STD_LOGIC);
end component halfsub;

begin
ho : halfsub port map (ah=>af, bh=>bf, hsub=>x, hborrow=>y);
h1 : halfsub port map (ah=>x, bh=>fborrowin, hsub=>fsub, hborrow=>z);
fborrowout <= y or z ;

end Behavioral;
----------------------------------------------end full sub------------------------------------------------------------------


----------------------------------------------4 bit sub----------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity forbtsub is
    Port ( a : in  STD_LOGIC_vector (3 downto 0);
           b : in  STD_LOGIC_vector (3 downto 0);
           sub : out  STD_LOGIC_vector (3 downto 0);
			  borrowin : in std_logic ;
           borrowout: out  STD_LOGIC);
end forbtsub;

architecture Behavioral of forbtsub is

signal l,m,n:std_logic ;
component fullsub  is 
port (     af : in  STD_LOGIC;
           bf : in  STD_LOGIC;
           fsub : out  STD_LOGIC;
			  fborrowin : in std_logic ;
           fborrowout: out  STD_LOGIC);
end component fullsub;

begin
f0 : fullsub port map (af=>a(0), bf=>b(0), fsub=>sub(0), fborrowout=>l, fborrowin=>borrowin );
f1 : fullsub port map (af=>a(1), bf=>b(1), fsub=>sub(1), fborrowout=>m, fborrowin=>l );
f2 : fullsub port map (af=>a(2), bf=>b(2), fsub=>sub(2), fborrowout=>n, fborrowin=>m );
f3 : fullsub port map (af=>a(3), bf=>b(3), fsub=>sub(3), fborrowout=>borrowout, fborrowin=>n );

end Behavioral;

---------------------------------------------end 4 bit sub ----------------------------------------------------------

