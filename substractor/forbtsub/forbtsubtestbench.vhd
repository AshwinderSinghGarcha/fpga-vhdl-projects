--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:16:16 12/24/2017
-- Design Name:   
-- Module Name:   E:/forbtsub/forbtsubtestbench.vhd
-- Project Name:  forbtsub
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: forbtsub
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY forbtsubtestbench IS
END forbtsubtestbench;
 
ARCHITECTURE behavior OF forbtsubtestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT forbtsub
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         sub : OUT  std_logic_vector(3 downto 0);
         borrowin : IN  std_logic;
         borrowout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');
   signal borrowin : std_logic := '0';

 	--Outputs
   signal sub : std_logic_vector(3 downto 0);
   signal borrowout : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: forbtsub PORT MAP (
          a => a,
          b => b,
          sub => sub,
          borrowin => borrowin,
          borrowout => borrowout
        );

--   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
a<="0101";
b<="1111";
borrowin<='0';
wait for 10 ns;	
a<="1001";
b<="0101";
borrowin<='0';
wait for 10 ns;	
a<="0001";
b<="0101";
borrowin<='0';
wait for 10 ns;	
a<="1101";
b<="0001";
borrowin<='0';
wait for 10 ns;	
a<="0111";
b<="0101";
borrowin<='0';
wait for 10 ns;	
a<="0101";
b<="0101";
borrowin<='0';

           

      -- insert stimulus here 

      wait;
   end process;

END;
