--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:59:04 12/24/2017
-- Design Name:   
-- Module Name:   E:/halfsub/halfsubtestbench.vhd
-- Project Name:  halfsub
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: halfsub
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY halfsubtestbench IS
END halfsubtestbench;
 
ARCHITECTURE behavior OF halfsubtestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT halfsub
    PORT(
         ah : IN  std_logic;
         bh : IN  std_logic;
         hsub : OUT  std_logic;
         hborrow : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal ah : std_logic := '0';
   signal bh : std_logic := '0';

 	--Outputs
   signal hsub : std_logic;
   signal hborrow : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   ---constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: halfsub PORT MAP (
          ah => ah,
          bh => bh,
          hsub => hsub,
          hborrow => hborrow
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
ah<='0';
bh<='0';
     wait for 10 ns;	
ah<='0';
bh<='1';
     wait for 10 ns;	
ah<='1';
bh<='0';
     wait for 10 ns;	
ah<='1';
bh<='1';
     

      -- insert stimulus here 

      wait;
   end process;

END;
