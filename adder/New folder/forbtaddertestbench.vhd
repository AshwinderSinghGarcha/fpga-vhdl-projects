--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:23:37 12/24/2017
-- Design Name:   
-- Module Name:   E:/forbtadder/forbtaddertestbench.vhd
-- Project Name:  forbtadder
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: forbtadder
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY forbtaddertestbench IS
END forbtaddertestbench;
 
ARCHITECTURE behavior OF forbtaddertestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT forbtadder
    PORT(
         A : IN  std_logic_vector(3 downto 0);
         B : IN  std_logic_vector(3 downto 0);
         sum : OUT  std_logic_vector(3 downto 0);
         cin : IN  std_logic;
         cout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(3 downto 0) := (others => '0');
   signal B : std_logic_vector(3 downto 0) := (others => '0');
   signal cin : std_logic := '0';

 	--Outputs
   signal sum : std_logic_vector(3 downto 0);
   signal cout : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: forbtadder PORT MAP (
          A => A,
          B => B,
          sum => sum,
          cin => cin,
          cout => cout
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
A<="1111";
B<="0001";
cin<='1';     
    wait for 10 ns;	
A<="1111";
B<="0011";
cin<='1';     

    wait for 10 ns;	
A<="1111";
B<="0111";
cin<='1';     
    wait for 10 ns;	
A<="1111";
B<="1001";
cin<='1';     
    wait for 10 ns;	
A<="1111";
B<="1101";
cin<='1';     

    wait for 10 ns;	
A<="1101";
B<="0001";
cin<='1';     


      --wait for <clock>_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
