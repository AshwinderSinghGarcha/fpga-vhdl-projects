----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:26:19 12/24/2017 
-- Design Name: 
-- Module Name:    forbtadder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity full is
port (af,bf,cinf:in std_logic;
      sumf,coutf:out STD_logic);
end full ;

architecture myadd of full is
begin

sumf <= af xor bf xor cinf ;
coutf <= ( af and bf ) or (cinf and (af xor bf));
end myadd;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity forbtadder is
    Port ( A : in  STD_LOGIC_VECTOR (3 downto 0);
           B : in  STD_LOGIC_VECTOR (3 downto 0);
           sum : out  STD_LOGIC_VECTOR (3 downto 0);
           cin : in  STD_LOGIC;
           cout : out  STD_LOGIC);
end forbtadder;

architecture Behavioral of forbtadder is

signal x,y,z :std_logic ;                     -- full adder component 
component full is 
port (af,bf,cinf:in std_logic;
      sumf,coutf:out STD_logic);
end component full ;


begin

  f0: full   port map (af=>A(0), bf=>B(0), cinf=>cin, sumf=>sum(0), coutf=>x   );
  f1: full   port map (af=>A(1), bf=>B(1), cinf=>x,   sumf=>sum(1), coutf=>y   );
  f2: full   port map (af=>A(2), bf=>B(2), cinf=>y,   sumf=>sum(2), coutf=>z   );
  f3: full   port map (af=>A(3), bf=>B(3), cinf=>z,   sumf=>sum(3), coutf=>cout);

end Behavioral;

