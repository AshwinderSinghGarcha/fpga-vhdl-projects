----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:25:27 12/09/2017 
-- Design Name: 
-- Module Name:    eightbitcomparator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity eightbitcomparator is
    Port ( a : in  STD_LOGIC_VECTOR (7 downto 0);
           b : in  STD_LOGIC_VECTOR (7 downto 0);
           equ : out  STD_LOGIC;
           grt : out  STD_LOGIC;
           less : out  STD_LOGIC);
end eightbitcomparator;

architecture Behavioral of eightbitcomparator is

begin
 process (a,b) 
                begin  
					 if (a=b) then 
					 equ <= '1';
					 grt <= '0';
					 less <='0';
					 elsif (a>b) then 
					 equ <= '0';
					 grt <= '1';
					 less <='0';
					 elsif (a<b) then 
					 equ <= '0';
					 grt <= '0';
					 less <='1';
					 end if;
					 end process;


end Behavioral;

