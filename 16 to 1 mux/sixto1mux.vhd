----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:57:27 12/16/2017 
-- Design Name: 
-- Module Name:    sixto1mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sixto1mux is
    Port ( in1 : in  STD_LOGIC_VECTOR (15 downto 0);
           in2 : in  STD_LOGIC_VECTOR (15 downto 0);
           in3 : in  STD_LOGIC_VECTOR (15 downto 0);
           in4 : in  STD_LOGIC_VECTOR (15 downto 0);
           in5 : in  STD_LOGIC_VECTOR (15 downto 0);
           in6 : in  STD_LOGIC_VECTOR (15 downto 0);
           in7 : in  STD_LOGIC_VECTOR (15 downto 0);
           in8 : in  STD_LOGIC_VECTOR (15 downto 0);
           in9 : in  STD_LOGIC_VECTOR (15 downto 0);
           in10 : in  STD_LOGIC_VECTOR (15 downto 0);
           in11 : in  STD_LOGIC_VECTOR (15 downto 0);
           in12 : in  STD_LOGIC_VECTOR (15 downto 0);
           in13 : in  STD_LOGIC_VECTOR (15 downto 0);
           in14 : in  STD_LOGIC_VECTOR (15 downto 0);
           in15 : in  STD_LOGIC_VECTOR (15 downto 0);
			  in16 : in  STD_LOGIC_VECTOR (15 downto 0);
           sel : in  STD_LOGIC_VECTOR (3 downto 0);
           en : in  STD_LOGIC;
           y : out  STD_LOGIC_VECTOR (15 downto 0));
end sixto1mux;

architecture Behavioral of sixto1mux is

begin
process (en ,sel)

begin 


     if (en = '1') then 
                    if (sel = "0000") then y<= in1 ;
					  elsif (sel = "0001") then y<= in2 ;
                 elsif (sel = "0010") then y<= in3 ;
                 elsif (sel = "0011") then y<= in4 ;
                 elsif (sel = "0100") then y<= in5 ;
                 elsif (sel = "0101") then y<= in6 ;
                 elsif (sel = "0110") then y<= in7 ;
                 elsif (sel = "0111") then y<= in8 ;
					  elsif (sel = "1000") then y<= in9 ;
					  elsif (sel = "1001") then y<= in10 ;
                 elsif (sel = "1010") then y<= in11;
                 elsif (sel = "1011") then y<= in12 ;
                 elsif (sel = "1100") then y<= in13 ;
                 elsif (sel = "1101") then y<= in14;
                 elsif (sel = "1110") then y<= in15 ;
                 elsif (sel = "1111") then y<= in16;
                 end if ; 
	 else 	
    y<= "ZZZZZZZZZZZZZZZZ";	 
     end if ;
end process ;	  
end Behavioral;



