----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:56:26 12/26/2017 
-- Design Name: 
-- Module Name:    addsub32bt - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity fulladdervhdl is
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           cin : in  STD_LOGIC;
           sum : out  STD_LOGIC;
           cout : out  STD_LOGIC);
end fulladdervhdl;

architecture Behavioral of fulladdervhdl is

begin
sum <= a xor b xor cin ;
cout <= ( a and b ) or (cin and (a xor b)) ;

end Behavioral;
--------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity addsub32bt is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           b : in  STD_LOGIC_VECTOR (31 downto 0);
           addsub : in  STD_LOGIC;
           z : out  STD_LOGIC_VECTOR (31 downto 0);
			  overflow : out  STD_LOGIC;
           coout : out  STD_LOGIC);
end addsub32bt;

architecture Behavioral of addsub32bt is
component fulladdervhdl is
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           cin : in  STD_LOGIC;
           sum : out  STD_LOGIC;
           cout : out  STD_LOGIC);
			  
end component fulladdervhdl;

signal carry :std_logic_vector (32 downto 0);
signal K    :std_logic_vector (31 downto 0);

begin

K(0)<=addsub xor b(0);
K(1)<=addsub xor b(1);
K(2)<=addsub xor b(2);
K(3)<=addsub xor b(3);
K(4)<=addsub xor b(4);
K(5)<=addsub xor b(5);
K(6)<=addsub xor b(6);
K(7)<=addsub xor b(7);
K(8)<=addsub xor b(8);
K(9)<=addsub xor b(9);
K(10)<=addsub xor b(10);
K(11)<=addsub xor b(11);
K(12)<=addsub xor b(12);
K(13)<=addsub xor b(13);
K(14)<=addsub xor b(14);
K(15)<=addsub xor b(15);
K(16)<=addsub xor b(16);
K(17)<=addsub xor b(17);
K(18)<=addsub xor b(18);
K(19)<=addsub xor b(19);
K(20)<=addsub xor b(20);
K(21)<=addsub xor b(21);
K(22)<=addsub xor b(22);
K(23)<=addsub xor b(23);
K(24)<=addsub xor b(24);
K(25)<=addsub xor b(25);
K(26)<=addsub xor b(26);
K(27)<=addsub xor b(27);
K(28)<=addsub xor b(28);
K(29)<=addsub xor b(29);
K(30)<=addsub xor b(30);
K(31)<=addsub xor b(31);

carry(0)<= addsub;
f0 :fulladdervhdl port map (a(0),K(0), carry(0)     ,z(0), carry(1));
f1 :fulladdervhdl port map (a(1),K(1), carry(1)     ,z(1), carry(2));
f2 :fulladdervhdl port map (a(2),K(2), carry(2)     ,z(2), carry(3));
f3 :fulladdervhdl port map (a(3),K(3), carry(3)     ,z(3), carry(4));


f4 :fulladdervhdl port map (a(4),K(4), carry(4)     ,z(4), carry(5));
f5 :fulladdervhdl port map (a(5),K(5), carry(5)     ,z(5), carry(6));
f6 :fulladdervhdl port map (a(6),K(6), carry(6)     ,z(6), carry(7));
f7 :fulladdervhdl port map (a(7),K(7), carry(7)     ,z(7), carry(8));


f8 :fulladdervhdl port map (a(8),K(8),  carry(8)     ,z(8), carry(9));
f9 :fulladdervhdl port map (a(9),K(9),  carry(9)     ,z(9), carry(10));
f10 :fulladdervhdl port map (a(10),K(10), carry(10)     ,z(10), carry(11));
f11 :fulladdervhdl port map (a(11),K(11), carry(11)     ,z(11), carry(12));


f12 :fulladdervhdl port map (a(12),K(12), carry(12)     ,z(12), carry(13));
f13 :fulladdervhdl port map (a(13),K(13), carry(13)     ,z(13), carry(14));
f14 :fulladdervhdl port map (a(14),K(14), carry(14)     ,z(14), carry(15));
f15 :fulladdervhdl port map (a(15),K(15), carry(15)     ,z(15), carry(16));


f16 :fulladdervhdl port map (a(16),K(16), carry(16)    ,z(16), carry(17));
f17 :fulladdervhdl port map (a(17),K(17), carry(17)     ,z(17), carry(18));
f18 :fulladdervhdl port map (a(18),K(18), carry(18)     ,z(18), carry(19));
f19 :fulladdervhdl port map (a(19),K(19), carry(19)     ,z(19), carry(20));


f20 :fulladdervhdl port map (a(20),K(20), carry(20)      ,z(20), carry(21));
f21 :fulladdervhdl port map (a(21),K(21), carry(21)     ,z(21), carry(22));
f22 :fulladdervhdl port map (a(22),K(22), carry(22)     ,z(22), carry(23));
f23 :fulladdervhdl port map (a(23),K(23), carry(23)     ,z(23), carry(24));


f24 :fulladdervhdl port map (a(24),K(24), carry(24)      ,z(24), carry(25));
f25 :fulladdervhdl port map (a(25),K(25), carry(25)     ,z(25), carry(26));
f26 :fulladdervhdl port map (a(26),K(26), carry(26)     ,z(26), carry(27));
f27 :fulladdervhdl port map (a(27),K(27), carry(27)     ,z(27), carry(28));


f28 :fulladdervhdl port map (a(28),K(28), carry(28)      ,z(28), carry(29));
f29 :fulladdervhdl port map (a(29),K(29), carry(29)     ,z(29), carry(30));
f30 :fulladdervhdl port map (a(30),K(30), carry(30)     ,z(30), carry(31));
f31 :fulladdervhdl port map (a(31),K(31), carry(31)     ,z(31), carry(32));






coout <= carry(32);
overflow <= carry(31) xor carry(32);


end Behavioral;

