--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:22:48 12/26/2017
-- Design Name:   
-- Module Name:   E:/addsub32bt/addsub32bttestbench.vhd
-- Project Name:  addsub32bt
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: addsub32bt
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY addsub32bttestbench IS
END addsub32bttestbench;
 
ARCHITECTURE behavior OF addsub32bttestbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT addsub32bt
    PORT(
         a : IN  std_logic_vector(31 downto 0);
         b : IN  std_logic_vector(31 downto 0);
         addsub : IN  std_logic;
         z : OUT  std_logic_vector(31 downto 0);
         overflow : OUT  std_logic;
         coout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(31 downto 0) := (others => '0');
   signal b : std_logic_vector(31 downto 0) := (others => '0');
   signal addsub : std_logic := '0';

 	--Outputs
   signal z : std_logic_vector(31 downto 0);
   signal overflow : std_logic;
   signal coout : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 --constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: addsub32bt PORT MAP (
          a => a,
          b => b,
          addsub => addsub,
          z => z,
          overflow => overflow,
          coout => coout
        );

--   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	

a<="10000100001000011000010000100001";
b<="00010001000100010001000110010001";
addsub<='1';
wait for 10 ns;	

a<="00000100001000011000010000100001";
b<="00010001000100010001000100010001";
addsub<='1';

wait for 10 ns;	

a<="10000100001000011000010000100001";
b<="00010001000100010001000110010001";
addsub<='0';

wait for 10 ns;

a<="00000100001000011000010000100001";
b<="00010001000100010001000100010001";
addsub<='1';


      -- insert stimulus here 

      wait;
   end process;

END;
