----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:39:35 12/06/2017 
-- Design Name: 
-- Module Name:    fourbitcomparator - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fourbitcomparator is
    Port ( a : in  STD_LOGIC_VECTOR (3 downto 0);
           b : in  STD_LOGIC_VECTOR (3 downto 0);
           eq : out  STD_LOGIC;
           less : out  STD_LOGIC;
           grt : out  STD_LOGIC);
end fourbitcomparator;

architecture Behavioral of fourbitcomparator is

begin
process (a,b)
begin 
if (a > b ) then 
less <= '0';
eq <= '0';
grt <= '1';
elsif (a< b) then    
less <= '1';
eq <= '0';
grt <= '0';
elsif (a=b)   then   
less <= '0';
eq <= '1';
grt <= '0';
end if;
end process; 


end Behavioral;

