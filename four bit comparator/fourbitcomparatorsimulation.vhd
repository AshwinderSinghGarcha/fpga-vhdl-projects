--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:42:17 12/06/2017
-- Design Name:   
-- Module Name:   E:/fourbitcomparator/fourbitcomparatorsimulation.vhd
-- Project Name:  fourbitcomparator
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: fourbitcomparator
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY fourbitcomparatorsimulation IS
END fourbitcomparatorsimulation;
 
ARCHITECTURE behavior OF fourbitcomparatorsimulation IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT fourbitcomparator
    PORT(
         a : IN  std_logic_vector(3 downto 0);
         b : IN  std_logic_vector(3 downto 0);
         eq : OUT  std_logic;
         less : OUT  std_logic;
         grt : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(3 downto 0) := (others => '0');
   signal b : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal eq : std_logic;
   signal less : std_logic;
   signal grt : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
--   constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: fourbitcomparator PORT MAP (
          a => a,
          b => b,
          eq => eq,
          less => less,
          grt => grt
        );

--   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	
		a<="1111";
		b<="1111";
		wait for 10 ns;	
		a<="1111";
		b<="0000";
		wait for 10 ns;	
		a<="0000";
		b<="1111";

      
      -- insert stimulus here 

      wait;
   end process;

END;
